
#ifndef  PERSIST_H
#define PERSIST_H

#include <string>
#include <iostream>
 
/*
object that can read/write to/from some medium(for example a file)
*/
class Archive
{
public:
	Archive(bool isStoring = true) : _isStoring(isStoring) {}
	virtual ~Archive() {}

	virtual void write(const void* buffer, size_t length) {}
	virtual void read(void* buffer, size_t length) {}

	Archive& operator<<(const std::string& str);
	Archive& operator>>(std::string& str);

	Archive& operator<<(int val);
	Archive& operator>>(int& val);

	/*
	add more types if needed
	*/

	bool isStoring() const				{ return _isStoring; }
	void setDirection(bool isStoring)	{ _isStoring = isStoring; }

private:
	bool _isStoring;
};


class ArchiveFile : public Archive
{
public:
	ArchiveFile(std::iostream* stream) : _stream(stream) {}
	virtual ~ArchiveFile() {}

	virtual void write(const void *buffer, size_t length);
	virtual void read(void* buffer, size_t length);

private:
	std::iostream* _stream;
};

#endif