#ifndef  FINAL_STATE_H
#define  FINAL_STATE_H

#include "State.h"
#include <iostream>
#include<string>
 

class Accept_State : public State{

public:
	virtual ~Accept_State(){}

	Accept_State(const std::string & seq_name) : State( 1), seq_name(seq_name){}

	virtual const id_type	handle_event(const Event& ev)const	{throw Emergency(seq_name, ev);}
	virtual void			serialize(Archive& stream);
	static	Accept_State*	createEmpty()						{ return new Accept_State; }

private:
	Accept_State() :State(1){}
	std::string seq_name;

};

#endif