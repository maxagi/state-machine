#include "Machine.h"
#include <iostream>
#include <fstream>
#include <iostream>

using namespace  std;

int main()
{

	//prepare the states
	Mid_State s1 = Mid_State();
	Mid_State s2 = Mid_State();
	Mid_State s3 = Mid_State();
	Mid_State s4 = Mid_State();
	Mid_State s5 = Mid_State();
	Mid_State s9 = Mid_State();

	Mid_State s6 = Mid_State(1000, Event::EV_C);

	Accept_State s7 = Accept_State("Seq1");
	Accept_State s8 = Accept_State("Seq2");
	Accept_State s10 = Accept_State("Seq3");

	 
	//add the transitions
	s1.add_transition(Event::EV_A, &s2);
	s2.add_transition(Event::EV_A, &s3);
	s3.add_transition(Event::EV_B, &s4);
	s4.add_transition(Event::EV_C, &s5);
	s5.add_transition(Event::EV_A, &s7);

	s2.add_transition(Event::EV_B, &s6);
	s6.add_transition(Event::EV_A, &s8);

	s2.add_transition(Event::EV_C, &s9);
	s9.add_transition(Event::EV_ANY, &s10);

	//put all mid states in a list
	Machine::Mid_State_List mid_states;
	mid_states.push_back(s1);
	mid_states.push_back(s2);
	mid_states.push_back(s3);
	mid_states.push_back(s4);
	mid_states.push_back(s5);
	mid_states.push_back(s6);
	mid_states.push_back(s9);

	//put all accepting states in list 
	Machine::Accept_States_List accepting_states;
	accepting_states.push_back(s7);
	accepting_states.push_back(s8);
	accepting_states.push_back(s10);


	//init stuff for loading and saving Machine
	Machine* m2 = 0;
	fstream file("m1.machine", ios::out | ios::in | ios::binary | ios::trunc);
	ArchiveFile stream(&file);


	//initialize and run Machine
	Machine m1(s1, mid_states, accepting_states);

	try
	{
		m1.recieve_event(Event_A());
		m1.recieve_event(Event_A());
		m1.recieve_event(Event_B());

		//save the machine to file 
		m1.save_machine_state(stream);

		//load it from the file to another object:
		file.seekg(0, ios::beg);
		m2 = Machine::load_machine_state(stream);

		//continue execution with the loaded Machine object:
		m2->recieve_event(Event_C());
		m2->recieve_event(Event_A());
	}

	catch (Emergency e)
	{
		EmergencyHandler::handle(e);
	}


	catch (const char * error_msg)
	{
		cout << error_msg;
	}

	delete m2;
	file.close();

	//hold to see result
	int x; cin >> x;
}