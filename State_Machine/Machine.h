#ifndef  MACHINE_H
#define  MACHINE_H

#include <list>
#include<map>
 
#include "Mid_State.h"
#include "Accept_State.h"

class Machine {

public:
	typedef std::list<const Mid_State> Mid_State_List;
	typedef std::list<const Accept_State> Accept_States_List;

	virtual ~Machine(){}
	Machine(const State& start, const Mid_State_List& midStates, const Accept_States_List& accepting_states);
	
	void				recieve_event(const  Event& ev);

	virtual void		serialize(Archive& stream);
	void				save_machine_state(Archive& stream);
	static	Machine*	load_machine_state(Archive& stream);

private:
	Machine(){}
	Machine(const Machine&);
	Machine& operator=(const Machine&);

	Mid_State*			findMidState(const State::id_type& id);
	const Accept_State*	findAcceptingState(const State::id_type id)const ;

	typedef std::map<const State::id_type, Mid_State > Mid_State_Id_Table;

	State::id_type			curent_state_id;
	Accept_States_List		accepting_states;
	Mid_State_Id_Table		IdToMidState;
};


#endif