#ifndef MYSTATE_H
#define MYSTATE_H

#include <map>
#include "State.h"

 
/*
a simple State implementation, holds a transition table and
returns to the machine the next state that the machine has to go to
*/

class Mid_State : public State{

public:
	virtual ~Mid_State(){}
	Mid_State() :State(0), loops_left(0){}

	Mid_State(const int numOfLoops, const Event::Event_Type& eventType) :
		State(0), loops_left(numOfLoops), looping_on_type(eventType){}

	void					add_transition(const Event::Event_Type& eventType, const State* const toState);
	virtual const id_type	handle_event(const Event& ev)const;

	virtual void			serialize(Archive& stream);
	static	Mid_State*		createEmpty()				{ return new Mid_State; }

	static	Mid_State*		CreateState(const std::string name)
	{
		if (name == "Mid_State")
			return createEmpty();
		if (name == "Multi_State")
			return 0;
	}


private:
	typedef std::map<Event::Event_Type, State::id_type >  transition_table;		//maps an event to the next state id.
	transition_table	transitions;
	mutable int			loops_left;
	Event::Event_Type	looping_on_type;
};

#endif