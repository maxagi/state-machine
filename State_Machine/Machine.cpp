#include "Machine.h"
using namespace std;


Machine::Machine(const State&  start, const Mid_State_List& midStates, const Accept_States_List& accepting_states) :
curent_state_id(start.getId()), accepting_states(accepting_states)
{
	for (Mid_State_List::const_iterator mid_state = midStates.cbegin(); mid_state != midStates.cend(); ++mid_state)
	{
		IdToMidState.insert(make_pair(mid_state->getId(), *mid_state));
	}
}



Mid_State*	Machine::findMidState(const State::id_type& id)
{
	Mid_State_Id_Table::iterator found_state = IdToMidState.find(id);
	if (found_state == IdToMidState.end())
		throw "state id not found in Machine's mid-state list ";

	return &found_state->second;
}

void Machine::recieve_event(const  Event& ev)
{
	//get the current state:
	Mid_State* current_state = findMidState(curent_state_id);

	//update current state id :
	curent_state_id = current_state->handle_event(ev);

	const Accept_State* found_accepting_state = findAcceptingState(curent_state_id);

	//if current state is in accepting list , Accepting_State::process() is called , and throws a relevant 'Danger'  exception
	if (found_accepting_state)	found_accepting_state->handle_event(ev);
}


const Accept_State* Machine::findAcceptingState(const State::id_type id)const
{
	for (Accept_States_List::const_iterator it = accepting_states.cbegin(); it != accepting_states.cend(); ++it)
	{
		if (it->getId() == curent_state_id)
			return &(*it);
	}
	return 0;
}



void Machine::serialize(Archive& stream)
{
	if (stream.isStoring())
	{
		//save curent_state_id
		stream << curent_state_id;

		//save mid-states
		stream << IdToMidState.size();
		for (Mid_State_Id_Table::iterator id_state_pair = IdToMidState.begin(); id_state_pair != IdToMidState.end(); ++id_state_pair)
			id_state_pair->second.serialize(stream);

		//save accepting states
		stream << accepting_states.size();
		for (Accept_States_List::iterator astate = accepting_states.begin(); astate != accepting_states.end(); ++astate)
			astate->serialize(stream);
	}

	else
	{
		//load current state id :
		State::id_type id;
		stream >> id;
		curent_state_id = id;

		//load IdTomidState
		int numOfMidStates;
		stream >> numOfMidStates;

		while (numOfMidStates--)
		{
			string className;
			stream >> className;

			Mid_State* mstate = Mid_State::CreateState(className); //Mid_State::createEmpty();
			auto_ptr<Mid_State> mstate_ptr(mstate);		//use auto_ptr because serialize can throw
			mstate->serialize(stream);
			IdToMidState.insert(make_pair(mstate->getId(), *mstate ));
		}

		//load accepting states
		int numOfAcceptingStates;
		stream >> numOfAcceptingStates;

		while (numOfAcceptingStates--)
		{
			Accept_State* astate = Accept_State::createEmpty();
			auto_ptr<Accept_State> astate_ptr(astate);	//use auto_ptr because serialize can throw
			astate->serialize(stream);
			accepting_states.push_back(*astate);
		}
	}
}


Machine* Machine::load_machine_state(Archive& stream)
{
		Machine* m = new Machine;
		auto_ptr<Machine> m_ptr(m);		
		stream.setDirection(false);
		m->serialize(stream);		//serialize can throw
		m_ptr.release();
		return m;
}


void Machine::save_machine_state(Archive& stream)
{
	stream.setDirection(true);
	serialize(stream);
} 