#include "Mid_State.h"


using namespace std;


void Mid_State::add_transition(const Event::Event_Type& eventType, const State* const toState)
{
	transitions.insert(make_pair(eventType, toState->getId()));
}


const State::id_type Mid_State::handle_event(const Event& ev)const
{
	transition_table::const_iterator found;

	//if there is an <ANY> transition , use it.
	if ((found= transitions.find(Event::EV_ANY)) != transitions.cend())
		return found->second;

	//check if state is in loop on some event
	if (loops_left)
	{
		if (looping_on_type == ev.getType())
		{
			--loops_left;
			return this->state_id;
		}
		//state received a different event than the event it has to loop on
		else throw "Machine got Stuck";
	} 

	//else go by the transition table
	found = transitions.find(ev.getType());
	if (found == transitions.cend()) 
		throw "Machine got Stuck";				
	return found->second;
}


void	Mid_State::serialize(Archive& stream)
{
	if (stream.isStoring())	
	{	//saving to stream:

		//save transitions:
		stream << transitions.size();
		for (transition_table::const_iterator transition = transitions.cbegin(); transition != transitions.cend(); ++transition)
		{
			stream << transition->first;
			stream << transition->second;
		}
		//save other data members:
		stream << loops_left;
		stream << looping_on_type;
	}

	else					
	{	//loading from stream:

		//load transitions:
 		int numOfTransitions;
 		stream >> numOfTransitions;
 
 		while (numOfTransitions--)
 		{
 			int et;
 			State::id_type sid;
 			stream >> et;
 			stream >> sid;
			transitions.insert(std::make_pair(Event::Event_Type(et), sid));
 		}

		//load other data members:
		stream >> loops_left;

		int et;
		stream >> et;
		looping_on_type = Event::Event_Type(et);
	}

	//continue with base serialization
	State::serialize(stream);
}


